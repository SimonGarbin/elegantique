<?php

/**
 * Functions and addition of actions and filters.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Elegantique
 * @since Elegantique 1.0
 */
defined('ABSPATH') || exit;

/* ----------------------------------------
 * Styles and scripts
 * ----------------------------------------
 */

if (!function_exists('elegantique_enqueue_scripts')) {

    add_action('wp_enqueue_scripts', 'elegantique_enqueue_scripts');

    /**
     * Registers and enqueues styles and scripts.
     *
     * @since Elegantique 1.0
     *
     * @return void
     */
    function elegantique_enqueue_scripts() {

        wp_enqueue_style(
                'roboto-font',
                'https://fonts.googleapis.com/css?family=Roboto:300,400,600',
                array(),
                null,
                'all'
        );
        wp_enqueue_style(
                'eb-garamond-font',
                'https://fonts.googleapis.com/css?family=EB+Garamond:300,400,600',
                array(),
                null,
                'all'
        );
        wp_enqueue_style(
                'elegantique-main-style',
                get_template_directory_uri() . '/style.css',
                array(),
                wp_get_theme()->get('Version'),
                'all'
        );
        wp_enqueue_script('jquery');
        wp_enqueue_script(
                'elegantique-main-script',
                get_template_directory_uri() . '/assets/js/main.js',
                array('jquery'),
                false,
                true
        );
    }

}

if (!function_exists('elegantique_enqueue_admin_scripts')) {

    add_action('admin_enqueue_scripts', 'elegantique_enqueue_admin_scripts');

    /**
     * Registers and enqueues styles and scripts for all admin pages.
     *
     * @since Elegantique 1.0
     *
     * @return void
     */
    function elegantique_enqueue_admin_scripts() {

        wp_register_script(
                'elegantique-admin',
                get_template_directory_uri() . '/assets/js/admin.js',
                array('wp-color-picker'),
                false,
                true
        );

        wp_enqueue_script('elegantique-admin');
        wp_enqueue_style('wp-color-picker');
    }

}

/* ----------------------------------------
 * Setup
 * ----------------------------------------
 */

if (!function_exists('elegantique_setup')) {

    add_action('after_setup_theme', 'elegantique_setup');

    /**
     * Sets up theme defaults and adds support for WordPress features.
     *
     * @since Elegantique 1.0
     *
     * @return void
     */
    function elegantique_setup() {

        load_theme_textdomain(
                'elegantique',
                get_template_directory() . '/languages'
        );

        register_nav_menus(
                array(
                    'primary' => esc_html__('Primary menu', 'elegantique'),
                )
        );

        add_theme_support('title-tag');
        add_theme_support('post-thumbnails');
        add_theme_support('editor-styles');
        add_theme_support('custom-logo', array('size' => 'medium'));
    }

}

if (!function_exists('elegantique_init')) {

    add_action('init', 'elegantique_init');

    /**
     * Adds support for WordPress features to the post type 'page'.
     *
     * @since Elegantique 1.0
     *
     * @return void
     */
    function elegantique_init() {

        add_post_type_support('page', 'excerpt');

        register_taxonomy_for_object_type('post_tag', 'page');
        register_taxonomy_for_object_type('category', 'page');
    }

}

if (!function_exists('elegantique_register_sidebars')) {

    add_action('widgets_init', 'elegantique_register_sidebars');

    /**
     * Registers all sidebars.
     *
     * @since Elegantique 1.0
     *
     * @return void
     */
    function elegantique_register_sidebars() {

        register_sidebar(
                array(
                    'name' => __('Sidebar', 'elegantique'),
                    'id' => 'sidebar',
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget' => '</section>',
                    'before_title' => '<span class="widget-title">',
                    'after_title' => '</span>',
                )
        );

        register_sidebar(
                array(
                    'name' => __('Footer widget area', 'elegantique') . ' (1)',
                    'id' => 'footer_widget_area_1',
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget' => '</section>',
                    'before_title' => '<span class="widget-title">',
                    'after_title' => '</span>',
                )
        );

        register_sidebar(
                array(
                    'name' => __('Footer widget area', 'elegantique') . ' (2)',
                    'id' => 'footer_widget_area_2',
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget' => '</section>',
                    'before_title' => '<span class="widget-title">',
                    'after_title' => '</span>',
                )
        );

        register_sidebar(
                array(
                    'name' => __('Footer widget area', 'elegantique') . ' (3)',
                    'id' => 'footer_widget_area_3',
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget' => '</section>',
                    'before_title' => '<span class="widget-title">',
                    'after_title' => '</span>',
                )
        );
    }

}

if (!function_exists('elegantique_get_sidebar')) {

    /**
     * ...
     *
     * @since Elegantique 1.0
     *
     * @return void
     */
    function elegantique_get_sidebar($name) {

        if (!is_singular()) {
            return;
        }

        get_sidebar($name);
    }

}

/* ----------------------------------------
 * Navigation menu
 * ----------------------------------------
 */

if (!function_exists('elegantique_nav_menu')) {

    /**
     * ...
     *
     * @since Elegantique 1.0
     * 
     * @param ...   $menu   ...
     * @param ...   $type   ...
     *
     * @return void
     */
    function elegantique_nav_menu($menu, $type) {

        $menu_loc = get_nav_menu_locations();
        if (!$menu_loc || !isset($menu_loc[$menu])) {
            return;
        }

        $menu_obj = wp_get_nav_menu_object($menu_loc[$menu]);
        if (!$menu_obj || !isset($menu_obj->term_id)) {
            return;
        }

        $menu_items = wp_get_nav_menu_items($menu_obj->term_id);
        $structure = elegantique_get_nav_menu_structure($menu_items, 0);

        if ($menu == 'primary') {
            if ($type == 'bar') {
                echo elegantique_get_nav_bar_html($structure);
            } else if ($type == 'panel') {
                echo elegantique_get_nav_panel_html($structure);
            }
        }
    }

}

if (!function_exists('elegantique_get_nav_bar_html')) {

    /**
     * ...
     *
     * @since Elegantique 1.0
     *
     * @param ...   $structure  ...
     * @param ...   $parent_id  ...
     * 
     * @return string
     */
    function elegantique_get_nav_bar_html($structure, $parent_id = 0) {

        $html = '';

        if ($parent_id != 0) {
            $html .= '<ul class="sub-menu">';
        } else {
            $html .= '<ul>';
        }

        foreach ($structure as $item) {

            $children = $item->wpse_children;

            if ($children) {
                $li_class = ' class="menu-item has-sub-menu"';
            } else {
                $li_class = ' class="menu-item"';
            }

            $html .= sprintf('<li%s>', $li_class);
            $html .= sprintf(
                    '<a href="%2$s">%1$s</a>',
                    $item->title,
                    $item->url,
            );

            if ($children) {
                $html .= '<span class="sub-menu-arrow open">'
                        . '<i class="fa-angle-down fa fa-sm"></i>'
                        . '</span>';
                $html .= elegantique_get_nav_bar_html(
                        $children,
                        $item->ID,
                );
            }

            $html .= '</li>';
        }

        $html .= '</ul>';

        return $html;
    }

}

if (!function_exists('elegantique_get_nav_panel_html')) {

    /**
     * ...
     *
     * @since Elegantique 1.0
     *
     * @param ...   $structure      ...
     * @param ...   $parent_id      ...
     * @param ...   $parent_title   ...
     * 
     * @return string
     */
    function elegantique_get_nav_panel_html(
            $structure,
            $parent_id = 0,
            $parent_title = null,
    ) {

        $html = '';

        if ($parent_id != 0) {
            $html .= '<ul class="sub-menu">';
            $html .= '<li class="menu-item sub-menu-title">'
                    . sprintf('<span>%1$s</span>', $parent_title)
                    . '<span class="sub-menu-arrow back">'
                    . '<i class="fa-angle-left fa fa-lg"></i>'
                    . '</span>'
                    . '</li>';
        } else {
            $html .= '<ul>';
        }

        foreach ($structure as $item) {

            $children = $item->wpse_children;

            if ($children) {
                $li_class = ' class="menu-item has-sub-menu"';
            } else {
                $li_class = ' class="menu-item"';
            }

            $html .= sprintf('<li%s>', $li_class);
            $html .= sprintf(
                    '<a href="%2$s">%1$s</a>',
                    $item->title,
                    $item->url,
            );

            if ($children) {
                $html .= '<span class="sub-menu-arrow open">'
                        . '<i class="fa-angle-right fa fa-lg"></i>'
                        . '</span>';
                $html .= elegantique_get_nav_panel_html(
                        $children,
                        $item->ID,
                        $item->title,
                );
            }

            $html .= '</li>';
        }

        $html .= '</ul>';

        return $html;
    }

}

if (!function_exists('elegantique_get_nav_menu_structure')) {

    /**
     * ...
     *
     * Authors: @DSkinner, @ImmortalFirefly and @SteveEdson
     *
     * @param ...   $elements   ...
     * @param ...   $parent_id  ...
     * 
     * @link https://stackoverflow.com/a/28429487/2078474
     */
    function elegantique_get_nav_menu_structure(array &$items, $parent_id = 0) {

        $branch = array();

        foreach ($items as &$item) {

            if ($item->menu_item_parent == $parent_id) {
                $children = elegantique_get_nav_menu_structure($items, $item->ID);

                if ($children) {
                    $item->wpse_children = $children;
                }

                $branch[$item->ID] = $item;
                unset($item);
            }
        }

        return $branch;
    }

}

/* ----------------------------------------
 * Archive pages and excerpts
 * ----------------------------------------
 */

if (!function_exists('elegantique_excerpt_more')) {

    add_filter('excerpt_more', 'elegantique_excerpt_more');

    /**
     * Modifies the string in the 'more' link displayed at the end of an
     * excerpt.
     *
     * @since Elegantique 1.0
     *
     * @param string    $more   The string shown within the 'more' link.
     * @return string
     */
    function elegantique_excerpt_more($more) {

        $more = sprintf(
                ' ... <a class="read-more" href="%1$s">%2$s</a>',
                get_permalink(),
                __('read more', 'elegantique')
        );

        return $more;
    }

}

if (!function_exists('elegantique_current_author')) {

    /**
     * Returns the author on an author archive page.
     *
     * @link https://codex.wordpress.org/Author_Templates
     *
     * @since Elegantique 1.0
     *
     * @return string
     */
    function elegantique_current_author() {

        if (!is_archive()) {
            return '';
        }

        if (get_query_var('author_name')) {
            $user = get_user_by('slug', get_query_var('author_name'));
        } else {
            $user = get_userdata(get_query_var('author'));
        }

        $curauth = $user ? $user->display_name : null;

        return $curauth;
    }

}

if (!function_exists('elegantique_excerpt_page_ancestors')) {

    /**
     * Displays the page ancestors in an excerpt.
     *
     * @since Elegantique 1.0
     *
     * @return void
     */
    function elegantique_excerpt_page_ancestors() {

        $ancestors = array_reverse(
                get_post_ancestors(get_the_ID())
        );

        $list = '';

        for ($i = 0; $i < count($ancestors); $i++) {

            $ancestor = $ancestors[$i];
            $title = esc_html(get_the_title($ancestor));
            $url = esc_url(get_permalink($ancestor));

            $list .= '<a href="' . $url . '">' . $title . '</a> &raquo; ';
        }

        $title = esc_html(get_the_title(get_the_ID()));
        $url = esc_url(get_permalink(get_the_ID()));

        $list .= '<a href="' . $url . '">' . $title . '</a>';

        echo $list;
    }

}

if (!function_exists('elegantique_excerpt_single_taxonomies')) {

    /**
     * Prints HTML with information about the (single) post taxonomy (i.e.
     * categories and tags).
     *
     * @since Elegantique 1.0
     *
     * @return void
     */
    function elegantique_excerpt_single_taxonomies() {

        if (has_category() || has_tag()) {

            $taxonomies = '<div class="excerpt-post-taxonomies">';

            $categories = get_the_category_list(wp_get_list_item_separator());
            if ($categories) {
                $taxonomies .= '<div class="excerpt-categories">'
                        . sprintf(
                                esc_html__('Categories: %s', 'antique'),
                                $categories,
                        )
                        . '</div>';
            }

            $tags = get_the_tag_list(wp_get_list_item_separator());
            if ($tags) {
                $taxonomies .= '<div class="excerpt-tags">'
                        . sprintf(
                                esc_html__('Tags: %s', 'antique'),
                                $tags,
                        )
                        . '</div>';
            }

            echo $taxonomies;
        }
    }

}

/* ----------------------------------------
 * Pagination
 * ----------------------------------------
 */

if (!function_exists('elegantique_posts_pagination')) {

    /**
     * Displays a paginated navigation to the next/previous posts.
     *
     * @link https://developer.wordpress.org/themes/functionality/pagination/
     *
     * @since Elegantique 1.0
     *
     * @return void
     */
    function elegantique_posts_pagination() {

        if (!is_paged() && !is_search()) {
            return;
        }

        the_posts_pagination(
                array(
                    'prev_next' => true,
                    'prev_text' => '&laquo;',
                    'next_text' => '&raquo;',
                    'end_size' => 1,
                    'mid_size' => 1,
                    'class' => 'pagination',
                )
        );
    }

}

if (!function_exists('elegantique_post_pagination')) {

    /**
     * Prints the page navigation for a post that is divided into multiple
     * pages.
     *
     * @link https://developer.wordpress.org/themes/functionality/pagination/
     *
     * @since Elegantique 1.0
     *
     * @return void
     */
    function elegantique_post_pagination() {

        global $numpages;

        if (!($numpages > 1)) {
            return;
        }

        $pagination = sprintf(
                '<nav class="post-navigation pagination" aria-label="%s">',
                esc_html__('pages'),
        );
        $pagination .= sprintf(
                '<h2 class="screen-reader-text">%s</h2>',
                esc_html__('Page navigation'),
        );
        $pagination .= '<div class="post-nav-links">';
        $pagination .= wp_link_pages(
                array(
                    'before' => '',
                    'after' => '',
                    'echo' => false,
                )
        );
        $pagination .= '</div>';
        $pagination .= '</nav>';

        echo $pagination;
    }

}

/* ----------------------------------------
 * Admin bar
 * ----------------------------------------
 */

if (!function_exists('elegantique_display_admin_bar')) {

    add_action('init', 'elegantique_display_admin_bar');

    /**
     * Displays the admin bar if the user is logged in.
     *
     * @since Elegantique 1.0
     *
     * @return void
     */
    function elegantique_display_admin_bar() {

        if (is_user_logged_in()) {
            add_filter('show_admin_bar', '__return_true', 1000);
        }
    }

}
