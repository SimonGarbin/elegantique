<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Elegantique
 * @since Elegantique 1.0
 */
?>

<?php get_header(); ?>

<div class="site-content">
    <header class="post-header">
        <h1 class="title post-title"><?php esc_html_e('Error 404', 'elegantique'); ?></h1>
        <img class="title-ornament"
             src="<?php
             echo get_template_directory_uri();
             ?>/assets/images/ornament.svg">
    </header>
    <p><?php
        printf(
                esc_html__(
                        'This page does not exist. Return to %s?',
                        'elegantique'
                ),
                sprintf(
                        '<a href="%s">%s</a>',
                        esc_url(get_home_url()),
                        esc_html__('homepage', 'elegantique')
                )
        );
        ?></p>

    <?php get_search_form(); ?>
</div>

<?php get_footer(); ?>