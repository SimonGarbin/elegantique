<?php
/**
 * Template part for displaying a post excerpt.
 *
 * @package Elegantique
 * @since Elegantique 1.0
 */
?>

<article id="post-<?php esc_attr(the_ID()); ?>" class="excerpt">
    <header class="excerpt-header">
        <h2 class="excerpt-title"><?php the_title(); ?></h2>
        <div class="excerpt-title-ruler"></div>
    </header>
    <div class="excerpt-content-wrap">
        <section class="excerpt-content"><?php
            wp_strip_all_tags(the_excerpt(), false);
            ?></section>
        <?php elegantique_excerpt_single_taxonomies(); ?>
    </div>
</article>