<?php
/**
 * The template for displaying sidebars.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/
 *
 * @package Elegantique
 * @since Elegantique 1.0
 */
?>

<?php if (is_singular() && is_active_sidebar('sidebar')) : ?>
    <aside class="site-sidebar">
        <?php dynamic_sidebar('sidebar'); ?>
    </aside>
<?php endif; ?>