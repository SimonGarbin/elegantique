<?php
/**
 * Template part for displaying the site navigation panel, which can be opened
 * on smaller screens.
 *
 * @package Elegantique
 * @since Elegantique 1.0
 */
?>

<?php if (has_nav_menu('primary')) : ?>

    <div class="dark-overlay nav-panel-toggle fade-out" aria-hidden="true">
    </div>

    <div class="site-navigation-panel">
        <nav class="navigation-panel" 
             aria-label="<?php esc_attr_e('Primary menu', 'elegantique'); ?>">
                 <?php elegantique_nav_menu('primary', 'panel'); ?>
        </nav>
    </div>

<?php endif; ?>