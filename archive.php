<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Creating_an_Archive_Index
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#category
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#tag
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#author-display
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#author-display
 *
 * @package Elegantique
 * @since Elegantique 1.0
 */
?>

<?php get_header(); ?>

<div class="site-content">
    <header class="content-header post-header">
        <h1 class="title archive-title"><?php
            if (is_category()) {
                printf(
                        __('Category: %s', 'elegantique'),
                        esc_html(single_cat_title('', false))
                );
            } else if (is_tag()) {
                printf(
                        __('Tag: %s', 'elegantique'),
                        esc_html(single_tag_title('', false))
                );
            } else if (is_date()) {
                printf(
                        __('Posts from %s', 'elegantique'),
                        get_the_date('F Y')
                );
            } else if (is_author()) {
                printf(
                        __('Posts by %s', 'elegantique'),
                        esc_html(elegantique_current_author())
                );
            } else {
                esc_html_e('Archive', 'elegantique');
            }
            ?></h1>
        <img class="title-ornament"
             src="<?php
             echo get_template_directory_uri();
             ?>/assets/images/ornament.svg">
    </header>
    <?php
    if (have_posts()) {
        while (have_posts()) {
            the_post();
            get_template_part('template-parts/excerpt/excerpt');
        }
        elegantique_posts_pagination();
    }
    ?>
</div>

<?php get_footer(); ?>