<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Elegantique
 * @since Elegantique 1.0
 */
?>

<?php get_header(); ?>

<div class="site-content">
    <header class="content-header post-header">
        <h1 class="title search-title"><?php
            printf(
                    esc_html__('Results for “%s”', 'elegantique'),
                    get_search_query()
            );
            ?></h1>
        <img class="title-ornament"
             src="<?php
             echo get_template_directory_uri();
             ?>/assets/images/ornament.svg">
    </header>
    <p><?php
        $found_results = (int) $wp_query->found_posts;
        if ($found_results == 0) {
            esc_html_e(
                    'There are no results for your search.',
                    'elegantique'
            );
        } else if ($found_results == 1) {
            esc_html_e(
                    'There is 1 result for your search:',
                    'elegantique'
            );
        } else {
            printf(
                    esc_html__(
                            'There are %d results for your search:',
                            'elegantique'
                    ),
                    (int) $wp_query->found_posts
            );
        }
        ?></p>
    <?php
    if (have_posts()) {
        while (have_posts()) {
            the_post();
            get_template_part('template-parts/excerpt/excerpt');
        }
        elegantique_posts_pagination();
    }
    ?>
</div>

<?php get_footer(); ?>
