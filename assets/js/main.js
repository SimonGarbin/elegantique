jQuery(document).ready(function ($) {

    // --- Functions ---
    function isMobile() {
        if ($(window).width() <= 768) {
            return true;
        } else {
            return false;
        }
    }
    
    function getWindowHeight() {
        return window.innerHeight;
    }

    function getMaximum() {
        return $(document).height() - getWindowHeight();
    }

    function getPosition() {
        var val = $(window).scrollTop();
        if (val > 0) {
            return val;
        } else {
            return 0;
        }
    }

    // --- Reading progress bar ---
    var $progressBar = $('#reading-progress-bar');

    $progressBar.attr({max: getMaximum()});
    $progressBar.attr({value: getPosition()});

    $(document).on('scroll', function () {
        $progressBar.attr({value: getPosition()});
    });

    $(window).on('resize touchmove', function () {
        $progressBar.attr({
            max: getMaximum(),
            value: getPosition()
        });
    });

    // --- Site header ---
    function setScrollPaddingTop() {
        $(':root').css({'scroll-padding-top': $('.site-header').outerHeight()});
    }

    $(window).on('resize', function () {
        setScrollPaddingTop();
    });

    var scrolled;
    var lastScrollPos = 0;
    var diffTrigger = $('.site-header').outerHeight();

    function moveSiteHeader() {

        if (isMobile()) {
            return;
        }

        var scrollPos = getPosition();
        if (Math.abs(lastScrollPos - scrollPos) <= diffTrigger) {
            return;
        }

        var siteHeaderHeight = $('.site-header').outerHeight();
        var siteTitleHeight = $('.site-branding').outerHeight();

        if (scrollPos > lastScrollPos && scrollPos > siteHeaderHeight) {
            $('.site-header').css({'top': '-' + siteTitleHeight + 'px'});
        } else {
            if (scrollPos + $(window).height() < $(document).height()) {
                $('.site-header').css({'top': '0px'});
            }
        }

        lastScrollPos = scrollPos;
    }

    $(window).scroll(function () {
        scrolled = true;
    });

    setInterval(function () {
        if (scrolled) {
            moveSiteHeader();
            scrolled = false;
        }
    }, 250);

    // --- Navigation panel ---
    var navPanelDepth = 0;

    function moveNavPanel(incr) {

        if (!navPanelDepth && incr < 0) {
            return;
        }

        navPanelDepth += incr;
        var newTransform = 'translateX(' + navPanelDepth * (-70) + 'vw)';
        $('.navigation-panel').css({'transform': newTransform});
    }

    $('.nav-panel-toggle').click(function () {
        $('.menu-icon').toggleClass('is-cross');
        if (!$('.dark-overlay').hasClass('fade-in')) {
            $('.dark-overlay').css({'right': '0'});
        }
        $('.dark-overlay').toggleClass('fade-in fade-out');
    });

    var darkOverlay = document.getElementsByClassName('dark-overlay')[0];
    darkOverlay.addEventListener('transitionend', function () {
        if (this.classList.contains('fade-out')) {
            this.style.right = '100vw';
        }
        
        moveNavPanel(-navPanelDepth);
    });

    $('.site-navigation-panel .sub-menu-arrow.open').click(function () {
        $('.site-navigation-panel .sub-menu').removeClass('is-displayed');
        $(this).siblings('.sub-menu').toggleClass('is-displayed');
        moveNavPanel(1);
    });

    $('.site-navigation-panel .sub-menu-arrow.back').click(function () {
        moveNavPanel(-1);
    });

    // --- Back-to-top button ---
    function showBackToTop(threshold) {
        if (getPosition() > threshold) {
            $('#back-to-top').removeClass('disappear');
        } else {
            $('#back-to-top').addClass('disappear');
        }
    }

    showBackToTop(threshold = 500);

    $(document).on('scroll', function () {
        showBackToTop(threshold = 500);
    });

    $('#back-to-top').on('click', function () {
        $('html, body').animate({scrollTop: 0}, 'fast');
    });

    // --- Turn off resizing on text inputs ---
    $('input').on('touchstart', function () {
        $(window).off('resize');
    });

    $('textarea').on('touchstart', function () {
        $(window).off('resize');
    });
});