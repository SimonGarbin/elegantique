��          �      |      �     �     �       	          	   2     <     L     X     f     s     �     �     �  %   �  %   �  "   �  '        8     A  	   G  �  Q     !     (     7  
   E     P     f     t     �     �  
   �     �     �     �     �  &   �  )   	  #   2	  2   V	  
   �	     �	     �	                                                              	                         
          Archive Categories: %s Category: %s Error 404 Footer widget area Open menu Page navigation Posts by %s Posts from %s Primary menu Results for “%s” Sidebar Tag: %s Tags: %s There are %d results for your search: There are no results for your search. There is 1 result for your search: This page does not exist. Return to %s? homepage pages read more Project-Id-Version: Elegantique
PO-Revision-Date: 2023-11-09 18:44+0100
Last-Translator: 
Language-Team: Simon Garbin
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0.1
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __;_e;esc_attr__;esc_attr_e;esc_html__;esc_html_e;_n
X-Poedit-SearchPath-0: 404.php
X-Poedit-SearchPath-1: archive.php
X-Poedit-SearchPath-2: footer.php
X-Poedit-SearchPath-3: functions.php
X-Poedit-SearchPath-4: header.php
X-Poedit-SearchPath-5: index.php
X-Poedit-SearchPath-6: page.php
X-Poedit-SearchPath-7: search.php
X-Poedit-SearchPath-8: sidebar.php
X-Poedit-SearchPath-9: single.php
X-Poedit-SearchPath-10: template-parts/content/content-page.php
X-Poedit-SearchPath-11: template-parts/content/content-single.php
X-Poedit-SearchPath-12: template-parts/extras/progress-bar.php
X-Poedit-SearchPath-13: template-parts/header/site-header.php
X-Poedit-SearchPath-14: template-parts/header/site-nav.php
X-Poedit-SearchPath-15: template-parts/header/site-nav-panel.php
X-Poedit-SearchPath-16: template-parts/footer/widget-areas.php
X-Poedit-SearchPath-17: template-parts/excerpt/excerpt.php
 Archiv Kategorien: %s Kategorie: %s Fehler 404 Footer Widget-Bereich Menü öffnen Seitennavigation Posts von %s Posts vom %s Hauptmenü Ergebnisse für „%s“ Seitenleiste Tag: %s Tags: %s Es gibt %d Ergebnisse für Ihre Suche: Es gibt keine Ergebnisse für Ihre Suche. Es gibt 1 Ergebnis für Ihre Suche: Diese Seite existiert nicht. Zurückkehren zur %s? Startseite Seiten weiterlesen 