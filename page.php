<?php
/**
 * The template for displaying pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-page
 *
 * @package Antique
 * @since Antique 1.0
 */

get_header();

if (have_posts()):
    while (have_posts()):
        the_post();
        get_template_part('template-parts/content/content-page');
    endwhile;
endif;

get_footer();
