<?php
/**
 * Template part for displaying the widget areas in the footer.
 *
 * @package Elegantique
 * @since Elegantique 1.0
 */
?>

<div class="footer-widget-areas">

    <?php if (is_active_sidebar('footer_widget_area_1')): ?>
        <aside class="widget-area">
            <?php dynamic_sidebar('footer_widget_area_1'); ?>
        </aside>
    <?php endif; ?>

    <?php if (is_active_sidebar('footer_widget_area_2')): ?>
        <aside class="widget-area">
            <?php dynamic_sidebar('footer_widget_area_2'); ?>
        </aside>
    <?php endif; ?>

    <?php if (is_active_sidebar('footer_widget_area_3')): ?>
        <aside class="widget-area">
            <?php dynamic_sidebar('footer_widget_area_3'); ?>
        </aside>
    <?php endif; ?>

</div>