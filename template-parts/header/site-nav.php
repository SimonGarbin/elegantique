<?php
/**
 * Template part for displaying the site navigation bar.
 *
 * @package Elegantique
 * @since Elegantique 1.0
 */
?>

<?php if (has_nav_menu('primary')) : ?>

    <div class="site-navigation-bar">
        <nav class="site-navigation" 
             aria-label="<?php esc_attr_e('Primary menu', 'elegantique'); ?>">
                 <?php elegantique_nav_menu('primary', 'bar'); ?>
        </nav>
        <div class="site-icon-menu nav-panel-toggle"
             aria-label="<?php esc_attr_e('Open menu', 'elegantique'); ?>">
            <div class="menu-icon">
                <div class="icon-bar"></div>
                <div class="icon-bar"></div>
                <div class="icon-bar"></div>
            </div>
        </div>
    </div>

<?php endif; ?>