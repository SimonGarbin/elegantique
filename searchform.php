<?php
/**
 * The template for displaying the search form.
 *
 * Used any time that get_search_form() is called.
 *
 * @link https://developer.wordpress.org/reference/functions/get_search_form/
 *
 * @package Elegantique
 * @since Elegantique 1.0
 */
?>

<div class="site-search">
    <form role="search"
          method="get"
          class="search-form"
          action="<?php echo esc_url(home_url('/')); ?>"
          >

        <input type="search"
               name="s"
               id="<?php echo esc_attr(wp_unique_id('search-form-')); ?>"
               class="search-field"
               value="<?php echo get_search_query(); ?>"
               autocomplete="off"
               ><button type="submit" class="search-submit">
            <i class="fa-solid fa-magnifying-glass"></i>
        </button>
    </form>
</div>