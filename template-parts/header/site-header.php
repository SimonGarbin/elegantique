<?php
/**
 * Template part for displaying the site header.
 *
 * @package Elegantique
 * @since Elegantique 1.0
 */
?>

<header id="masthead" class="site-header">
    <div class="site-branding">
        <?php if (has_custom_logo()) : ?>
            <div class="site-logo"><?php the_custom_logo(); ?></div>
        <?php else: ?>
            <div class="site-title"><?php echo get_bloginfo('name'); ?></div>
        <?php endif; ?>
    </div>
    <?php get_template_part('template-parts/header/site-nav'); ?>
</header>