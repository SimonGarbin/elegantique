<?php
/**
 * Template part for displaying the single post content in single.php.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Elegantique
 * @since Elegantique 1.0
 */
?>

<div class="site-content">
    <article id="post-<?php the_ID(); ?>" <?php post_class('post-content'); ?>>
        <header class="content-header post-header">
            <h1 class="title post-title"><?php the_title(); ?></h1>
            <img class="title-ornament"
                 src="<?php
                 echo get_template_directory_uri();
                 ?>/assets/images/ornament.svg">
        </header>
        <?php the_content(); ?>
        <?php elegantique_post_pagination(); ?>
    </article>
</div>