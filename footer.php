<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of main and all the content after (and ends with the
 * closing tag </html> of the root).
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Elegantique
 * @since Elegantique 1.0
 */
?>

</main>
<?php get_sidebar(); ?>
</div>

<footer id="colophon" class="site-footer">
    <?php get_template_part('template-parts/footer/widget-areas'); ?>
    <div class="site-info">
        &copy; <?php echo date('Y'); ?> <?php echo get_bloginfo('name'); ?>
        | Powered by
        <a href="https://gitlab.com/wp-antique/antique"
           target="_blank"><?php echo wp_get_theme(); ?></a>
        Wordpress Theme
    </div>
</footer>

<div id="back-to-top">
    <i class="fa-solid fa-angle-up"></i>
</div>

</div>

<?php wp_footer(); ?>

</body>
</html>